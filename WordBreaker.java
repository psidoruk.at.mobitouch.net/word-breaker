import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class WordBreaker {

  private static final Integer ONE = 1;
  private static final String INPUT_FILE_PATH = "./in/batch.txt";
  private static final String OUTPUT_FILE_PATH = "./out/result.csv";
  private static final String EMPTY_STRING = "";
  private static final String SPLIT_REGEX = "[ \t]";
  private static final String NO_MEANING_CHARS = "[.,!?_()<>\"��']";
  private static final String HTTP_PREFIX = "http";

  public static void main(String[] args) throws IOException {
    Map<String, Integer> words = TextNumberMapTools.sortWordsByCount(countWordsInFile());
    try {
      Integer excludedWordLength = Integer.valueOf(args[0]);
      words = filterWordsShorterThan(excludedWordLength, words);
      words = filteNoMeaningWords(words);
      words = filterHttpUrls(words);
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("You did not filter any short words. Consider running app with number argument like this: \"java %APP_NAME% 3\" to cut all words that has length up to 3 chars.");
    }
    MapCsvTool.creteCsvFile(words, OUTPUT_FILE_PATH);
  }

  private static Map<String, Integer> filterHttpUrls(Map<String, Integer> words) {
    return words.entrySet().stream()
        .filter(entry -> !entry.getKey().startsWith(HTTP_PREFIX))
        .collect(Collectors.toMap(
            Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
  }

  private static Map<String, Integer> filteNoMeaningWords(Map<String, Integer> words) {
    return words.entrySet().stream()
        .filter(entry -> !NO_MEANING_WORDS.contains(entry.getKey()))
        .collect(Collectors.toMap(
            Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
  }

  private static Map<String, Integer> filterWordsShorterThan(Integer minWordLength, Map<String, Integer> words) {
    return words.entrySet().stream()
        .filter(entry -> entry.getKey().length() > minWordLength)
        .collect(Collectors.toMap(
            Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
  }

  private static Map<String, Integer> countWordsInFile() throws IOException {
    Map<String, Integer> wordsCount = new HashMap<>();
    File file = new File(INPUT_FILE_PATH);
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;

    while ((line = reader.readLine()) != null) {
      String[] words = line.split(SPLIT_REGEX);
      List<String> wordsFiltered = Arrays.stream(words)
          .filter(s -> !s.isEmpty())
          .map(s -> s.replaceAll(NO_MEANING_CHARS, EMPTY_STRING))
          .map(String::toLowerCase)
          .map(String::trim)
          .collect(toList());
      wordsFiltered.forEach(word -> wordsCount.merge(word, ONE, Integer::sum));
    }
    return wordsCount;
  }

  private static final List<String> NO_MEANING_WORDS = Arrays.asList(
      "je�li",
      "mo�na",
      "b�dzie",
      "przed",
      "przez",
      "prosimy",
      "prosz�",
      "kt�ra",
      "kt�ry",
      "kt�re",
      "tylko",
      "samym",
      "dzi�ki",
      "wtedy",
      "te�",
      "tak�e",
      "jest",
      "oraz",
      "mog�",
      "mog�",
      "ten",
      "tego",
      "mo�e",
      "mo�esz",
      "bardzo",
      "przy",
      "dobrze",
      "gdzie",
      "kiedy",
      "dobry",
      "mamy",
      "naszej",
      "jednak",
      "maj�",
      "r�wnie�",
      "wi�c",
      "twojej",
      "naszych",
      "dzi�kujemy",
      "dlaczego",
      "pozdrawiamy",
      "zach�camy",
      "dlatego",
      "wszystkich"
  );
}